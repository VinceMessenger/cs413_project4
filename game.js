(function(){var gameport = document.getElementById("gameport");

var game_width = 1504;
var game_length = 608;

var running = false;  // boolean flag for game running
var width = 608;
var length = 608;
var num_targets = 50;

var jumping = false;
var gravity = .1;
var jump_speed;
var jump_start;


var renderer = PIXI.autoDetectRenderer(width, length, {backgroundColor: 0x5b6ee1});
gameport.appendChild(renderer.view);

var entity_layer, tu, world, colliders;

var tut_text = new PIXI.Text("Use A and D keys to move your player.\nUse W to jump.\nUse the space bar to shoot.\nPick up powerups and \ntry to survive!\n\nPress M to mute/unmute all audio.\n\nPress ESC to return to the main menu.", {font: "18px Arial"});
tut_text.position.x = 175;
tut_text.position.y = 200;
var cred_text = new PIXI.Text("   Developed by Vince Messenger\n            Messenger Games\n\nPress ESC to return to the main menu.", {font: "18px Arial"});
cred_text.position.x = 175;
cred_text.position.y = 200;
var text = new PIXI.Text("");

var orig_tank_speed = 2.5;
var tank_speed = orig_tank_speed;
var orig_shoot_speed = 500;
var shoot_speed = orig_shoot_speed;
var last_shot;          // time of last fire
var scatter_shot = false;
var bullets = [];   // array for holding active bullets

var targets = [];   // array for holding targets

// winner var
var winner;

// boss vars
var angry_boss, angry_boss_start, bossRight, bossLeft, bossLastShot, angry_boss_warning, bubble, bubble2;
var bossShootSpeed = 1000; // one second
var boss_bullets = [];

// jetpack vars
var jetpack, jetpack_text;
var hasJetpack, thrusting;

// particle vars
var emitter, elapsed;

// vars for all the different game screens
var stage = new PIXI.Container();   // game screen
var start_screen = new PIXI.Container();   // start screen
var end_screen = new PIXI.Container();     // game over screen
var credit_screen = new PIXI.Container();  // credits screen
var tutorial_screen = new PIXI.Container();
var current_stage = start_screen;

// add background image
//var bg = new PIXI.Sprite(new PIXI.Texture.fromImage("background.png"));
var bg = new PIXI.Sprite(new PIXI.Texture.fromImage("background.png"));
var go_bg = new PIXI.Sprite(new PIXI.Texture.fromImage("background.png"));   // game_over background
var start_bg = new PIXI.Sprite(new PIXI.Texture.fromImage("background.png"));   // start_screen background
var tut_bg = new PIXI.Sprite(new PIXI.Texture.fromImage("background.png"));
var cred_bg = new PIXI.Sprite(new PIXI.Texture.fromImage("background.png"));

stage.addChild(bg);
end_screen.addChild(go_bg);
start_screen.addChild(start_bg);
tutorial_screen.addChild(tut_bg);
credit_screen.addChild(cred_bg);

// movieplayer
var frames = [];
var boss_frames = [];
var runner;

// create menu sprite animation
PIXI.loader
  .add("assets.json")
  .add("assets1.json")
  .load();

function ready() {

	for (var i=1; i<=11; i++) {
	    frames.push(PIXI.Texture.fromFrame('full_hangman' + i + '.png'));
	}

    runner = new PIXI.extras.MovieClip(frames);
	runner.position.x = 150;
	runner.position.y = 470;
	runner.animationSpeed = 0.1;
	runner.play();
	start_bg.addChild(runner);
}

// add text to screens
cred_bg.addChild(cred_text);
tutorial_screen.addChild(tut_text);

// powerup vars!
var spawned;
var last_powerup_spawn;   // time of last spawn
var cur_powerup;
var powerup_time;  // count down from 5 or so
var rapid_fire_sprite = new PIXI.Sprite(new PIXI.Texture.fromImage("rapid_fire.png"));
var scatter_shot_sprite = new PIXI.Sprite(new PIXI.Texture.fromImage("scatter_shot.png"));
var speed_boost_sprite = new PIXI.Sprite(new PIXI.Texture.fromImage("speed_boost.png"));
var god_mod_sprite = new PIXI.Sprite(new PIXI.Texture.fromImage("god_mode.png"));
var powerup_station = new PIXI.Sprite(new PIXI.Texture.fromImage("powerup_station.png"));
var scatter_shot;
var speed_boost;
var rapid_fire;
var god_mode;
var power_ups = ['rapid_fire', 'scatter_shot', 'speed_boost', 'god_mode'];   // array for randomly choosing a powerup to spawn


// vars for processInput()
var goRight, goLeft, falling;

// sound init
var theme, balloon_pop, jetpack_sound, gunshot_sound, muted;
PIXI.loader.add("balloon_pop.wav").add("jetpack_sound.wav").add("gunshot_sound.wav").add("theme.wav").load(sound_ready);

function sound_ready() {
	theme = PIXI.audioManager.getAudio("theme.wav");
	theme.loop = true;
	balloon_pop =  PIXI.audioManager.getAudio("balloon_pop.wav");
	jetpack_sound = PIXI.audioManager.getAudio("jetpack_sound.wav");
	jetpack_sound.loader = true;
	gunshot_sound = PIXI.audioManager.getAudio("gunshot_sound.wav");

	theme.volume = .35;
	theme.play();

	jetpack_sound.volume = .07;
	balloon_pop.volume = .1;
	gunshot_sound.volume = .18;
	console.log("theme vol: " + jetpack_sound.volume);

	muted = false;
}

PIXI.loader
  .add('map_json', 'map.json')
  .add('tileset', 'map.png')
  .add('full_bro', 'full_hangman.png')
  .add('jetpack', 'jetpack.png')
  .load();

 function call_main() {
 	main_menu();
 }

var menu, play, tutorial, credits;

// create start screen menu
menu = new PIXI.Container();
menu.position.x = 260;
menu.position.y = 220;
start_bg.addChild(menu);

// add play to menu
play = new PIXI.Text("Play", {font: "35px Arial"});
menu.addChild(play);
play.interactive = true;
play.mouseover = hover_start;
play.mouseout = hover_end;

// add tutorial to menu
tutorial = new PIXI.Text("Tutorial", {font: "35px Arial"});
menu.addChild(tutorial);
tutorial.interactive = true;
tutorial.mouseover = hover_start;
tutorial.mouseout = hover_end;
tutorial.position.y = 40;
tutorial.position.x = -22;

// add credits to menu
var credits = new PIXI.Text("Credits", {font: "35px Arial"});
menu.addChild(credits);
credits.interactive = true;
credits.mouseover = hover_start;
credits.mouseout = hover_end;
credits.position.y = 80;
credits.position.x = -17;

// tutorial click function
tutorial.click = function() {
	current_stage = tutorial_screen;
}

// credits click
credits.click = function() {
	current_stage = credit_screen;
}

// define hover functions
function hover_start() {
	this.scale.x = 1.25;
	this.scale.y = 1.25;
}

function hover_end() {
	this.scale.x = 1;
	this.scale.y = 1;
}

// play click function
play.click = function() {
	init();
}


function init() {

	current_stage = stage;

	winner = false;
	text.visible = false;


	tu = new TileUtilities(PIXI);
	world = tu.makeTiledWorld("map_json", "map.png");
  	stage.addChild(world);
  	entity_layer = world.getObject("GameObjects");

	var bro = world.getObject("bro");
	var jp = world.getObject("jetpack");
  
  	// movieplayer boss init
  	for (var i=1; i<=7; i++) {
		boss_frames.push(PIXI.Texture.fromFrame('full_boss' + i + '.png'));
	}
 	player = new PIXI.extras.MovieClip(boss_frames);
 	angry_boss = false;
 	angry_boss_start = false;
 	angry_boss_warning = false;
 	bossLeft = false;
 	bossRight = false;
 	bossLastShot = false;
  	player.x = bro.x;
  	player.y = bro.y;   
  	player.anchor.x = 0.5;
  	player.anchor.y = 1.0;
  	player.animationSpeed = .15;
  	jump_start = player.position.y;
  	//entity_layer.addChild(player);

  	// chat bubble init
  	bubble = new PIXI.Sprite(PIXI.Texture.fromImage("first_text.png"));
  	bubble2 = new PIXI.Sprite(PIXI.Texture.fromImage("2nd_text.png"));
  	bubble.position.x = player.position.x + 25;
  	bubble.position.y = player.position.y - 200;
  	bubble2.position.x = bubble.position.x;
  	bubble2.position.y = bubble.position.y;
  	bubble2.visible = false;
  	entity_layer.addChild(bubble);
  	entity_layer.addChild(bubble2);

  	// jetpack init
  	jumping = false;
  	hasJetpack = false;
  	thrusting = false;
  	jetpack = new PIXI.Sprite(PIXI.loader.resources.jetpack.texture);
  	jetpack.x = jp.x;
  	jetpack.y = jp.y;
  	jetpack.anchor.x = .45;
  	jetpack.anchor.y = 1.0;
  	//entity_layer.addChild(jetpack);

  	jetpack_text = new PIXI.Text("Jetpack", {font: "15px Arial"});
  	jetpack_text.position.x = jetpack.position.x-25;
  	jetpack_text.position.y = jetpack.position.y - 60;
  	entity_layer.addChild(jetpack_text);
  	
  	// movieplayer runner init
  	for (var i=1; i<=7; i++) {
	    frames.push(PIXI.Texture.fromFrame('full_hangman' + i + '.png'));
	}
    runner = new PIXI.extras.MovieClip(frames);
	runner.position.x = player.position.x + 100;
	runner.position.y = player.position.y;
	runner.anchor.x = player.anchor.x;
	runner.anchor.y = player.anchor.y;
	runner.scale.x = -1;
	runner.animationSpeed = 0.2;
	runner.play();
	//entity_layer.addChild(runner);

	// targets init and add to stage
	for (var i = 0; i < 30; i++) {
		targets[i] = new PIXI.Sprite(PIXI.Texture.fromImage("balloon.png"));
		targets[i].position.x = (Math.random() * ((game_width-70)-30)) + 30;
		targets[i].position.y = Math.random() * game_length/2;  // limit targets from going below half the screen
		var scale = Math.random() * 1.25 + .5;   // find a random scale for each target
		targets[i].scale.x = scale;
		targets[i].scale.y = scale;
		targets[i].radius = 25 * scale;  // set and scale radius
		entity_layer.addChild(targets[i]);
	}

	// powerup init
	rapid_fire = false;
	speed_boost = false;
	scatter_shot = false;
	god_mode = false;
	powerup_time = 0;

	//powerup_station init
	powerup_station.position.x = 1300;
	powerup_station.position.y = jump_start-100;
	powerup_station.position.z = runner.position.z - .1;

	entity_layer.addChild(powerup_station);
	entity_layer.addChild(runner);
	entity_layer.addChild(jetpack);
	entity_layer.addChild(player);

	// spawn powerup
	spawnPowerup();

	//emitter init
	emitter = new PIXI.particles.Emitter(
  	entity_layer,

  	[PIXI.Texture.fromImage('particle.png')],

  	// Emitter configuration; This was copied and pasted from the file
  	// downloaded from http://pixijs.github.io/pixi-particles-editor/
	  {
		"alpha": {
			"start": 1,
			"end": 0
		},
		"scale": {
			"start": 0.42,
			"end": 0.08,
			"minimumScaleMultiplier": 1
		},
		"color": {
			"start": "#db5844",
			"end": "#f5f523"
		},
		"speed": {
			"start": 112,
			"end": 50
		},
		"acceleration": {
			"x": 0,
			"y": 0
		},
		"startRotation": {
			"min": 79,
			"max": 360
		},
		"rotationSpeed": {
			"min": 1,
			"max": 1
		},
		"lifetime": {
			"min": 0.33,
			"max": 0.74
		},
		"blendMode": "normal",
		"frequency": 0.003,
		"emitterLifetime": -1,
		"maxParticles": 353,
		"pos": {
			"x": 0,
			"y": 0
		},
		"addAtBack": true,
		"spawnType": "rect",
		"spawnRect": {
			"x": 0,
			"y": 0,
			"w": 0,
			"h": 0
		}
	}
	);
	emitter.emit = false;

	elapsed = Date.now();

  	world.position.x = 0;

	running = true;  // restart game

}

function spawnPowerup() {
	cur_powerup = power_ups[Math.floor(Math.random() * power_ups.length)]; // choose random powerup
	if (cur_powerup == 'god_mode') {
		cur_powerup = power_ups[Math.floor(Math.random() * power_ups.length)]; // choose again to make god_mode more rare
	}

	//cur_powerup = 'god_mode';  // hard code for testing

	if (cur_powerup == 'rapid_fire') {
		cur_powerup = rapid_fire_sprite;
	}

	if (cur_powerup == 'scatter_shot') {
		cur_powerup = scatter_shot_sprite;
	}

	if (cur_powerup == 'speed_boost') {
		cur_powerup= speed_boost_sprite;
	}

	if (cur_powerup == 'god_mode') {
		cur_powerup = god_mod_sprite;
	}

	cur_powerup.position.x = 1350;
	cur_powerup.position.y = jump_start - 50;
	cur_powerup.anchor.x = .5;
	entity_layer.addChild(cur_powerup);
	cur_powerup.visible = true;
	spawned = true;

}

function update_camera() {
  //stage.x = -player.x + width/2 - player.width/2;
  //stage.x = -Math.max(0, Math.min(world.worldWidth - width, -stage.x));
  stage.x = -runner.x + width/2 - runner.width/2;
  stage.x = -Math.max(0, Math.min(world.worldWidth - width, -stage.x));
}

// define keydownListener
function keydownListener(key) {

	if (key.keyCode == 65) {   // a key
		if (running) {
			//player.scale.x = -1;
			goLeft = true;
			runner.scale.x = -1;
			runner.play();
		}
	}

	if (key.keyCode == 68) {   // d key
		if (running) {
			//player.scale.x = 1;
			goRight = true;
			runner.scale.x = 1;
			runner.play();
		}
	}
	
	if (key.keyCode == 87 && running) {   // W key
		key.preventDefault();

		if(!jumping) {
			jumping = true;
			jump_speed = 3;
		}

		if (hasJetpack) {
			thrusting = true;   // turn on jetpack thrusters
			emitter.emit = true;  // start flame emitter
			jetpack_sound.play();
		}
	}

	if (key.keyCode == 27 && !running) {    // esc
		current_stage = start_screen;
	}

	if (key.keyCode == 13 && !running && current_stage == stage) {   // enter
		init();
	}

	if (key.keyCode == 32 && running) {   // space
		key.preventDefault();
		shoot();
	}

	if (key.keyCode == 77){   // M key
		if (muted) {
			PIXI.audioManager.unmute();
			muted = false;
		}
		else {
			PIXI.audioManager.mute();
			muted = true;
		}
	}
}

// define keyupListener
function keyupListener(key) {
	if (key.keyCode == 65) {   // a key
		goLeft = false;
		
		if (!goRight)
			runner.gotoAndStop(0);
	}

	if (key.keyCode == 68) {   // d key
		goRight = false;

		if (!goLeft)
			runner.gotoAndStop(0);
	}

	if (key.keyCode == 87 && running) {   // W key
		if (hasJetpack){
			thrusting = false;   // turn off jetpack thrusters
			emitter.emit = false; // turn off flame emitter
			jetpack_sound.stop();
		}
	}

	if (key.keyCode == 13 && running) {   // enter
		key.preventDefault();
	}

}

function shoot() {
	if (!last_shot || new Date().getTime() - last_shot >= shoot_speed) {
	  	last_shot = new Date().getTime();   // get time of shot
	    
	    if (scatter_shot) {
	    	for (var i = 0; i < 3; i++){
	    		bullets[bullets.length] = new PIXI.Sprite(PIXI.Texture.fromImage("bullet.png"));
				bullets[bullets.length-1].visible = true;  // must use bullets.length-1 after adding to array
				bullets[bullets.length-1].position.x = runner.position.x + (runner.scale.x * 30);
				bullets[bullets.length-1].position.y = runner.position.y-58;
				bullets[bullets.length-1].radius = 5;
				entity_layer.addChild(bullets[bullets.length-1]);
				createjs.Tween.get(bullets[bullets.length-1].position).to({x: bullets[bullets.length-1].position.x + (runner.scale.x * 1600), y: bullets[bullets.length-1].position.y - 85 + i *85}, 2000, createjs.Ease.linear);
				gunshot_sound.play();
	    	}
	    }
	    else {
			bullets[bullets.length] = new PIXI.Sprite(PIXI.Texture.fromImage("bullet.png"));
			bullets[bullets.length-1].visible = true;  // must use bullets.length-1 after adding to array
			bullets[bullets.length-1].position.x = runner.position.x + (runner.scale.x * 30);
			bullets[bullets.length-1].position.y = runner.position.y-58;
			bullets[bullets.length-1].radius = 5;
			entity_layer.addChild(bullets[bullets.length-1]);
			createjs.Tween.get(bullets[bullets.length-1].position).to({x: bullets[bullets.length-1].position.x + (runner.scale.x * 1600)}, 2000, createjs.Ease.linear);
			gunshot_sound.play();
		}
	}
}

function checkCollisions() {
	// Check intersection between bullets and targets:
    for (var i in bullets) {
    	for (var j in targets) {
	      	if (bullets[i].visible == true && circle_intersection(bullets [i], targets[j]) && targets[j].visible == true) {
	        	bullets[i].visible = false;
	        
	        	// hide target
	        	targets[j].visible = false;
	        	targets[j].position.y = -100;   // move invisible target off screen to aid collision detection
	        	targets.splice(j, 1); // remove target that has been hit
	        	balloon_pop.play();

	        	if (targets.length == 0)
	        		angry_boss = true;   // turn on boss mode
	    	}
	    }

	    // check bullets against boss
	    if (angry_boss_start && new Date().getTime() - angry_boss_warning >= 5000) {
	    	if (Math.abs(bullets[i].position.x - player.position.x) <= 30) {
	    		if (bullets[i].position.y >= player.position.y - 120 && bullets[i].position.y <= player.position.y) {
    				bullets[i].visible = false;

    				winner = runner;   // player wins, ingnore var name
    				    				
    				player.texture = new PIXI.Texture.fromImage('dead.png');
    				createjs.Tween.get(player).to({rotation: (-player.scale.x * 1.57)}, 1000, createjs.Ease.bounceOut);
    				//game_over();
				}
	    	}
	    }
    }

    if (angry_boss_start && new Date().getTime() - angry_boss_warning >= 5000) {
    	for (var i in boss_bullets) {
    		if (Math.abs(boss_bullets[i].position.x - runner.position.x) <= 30) {
    			if (boss_bullets[i].position.y >= runner.position.y - 120 && boss_bullets[i].position.y <= runner.position.y) {
    				boss_bullets[i].visible = false;

    				winner = player;   // boss wins, ingnore var name

    				runner.texture = new PIXI.Texture.fromImage('dead.png');
    				createjs.Tween.get(runner).to({rotation: (player.scale.x * 1.57)}, 1000, createjs.Ease.bounceOut);
    				createjs.Tween.get(runner).to({y: jump_start}, 1000, createjs.Ease.bounceOut);
    				createjs.Tween.get(jetpack).to({y: jump_start + 5}, 1000, createjs.Ease.bounceOut);
    				//game_over();
				}
    		}
    	}
    }

    // check collision with jetpack
    if (Math.abs(runner.position.x - jetpack.position.x) < 22 && jetpack_text.visible) {
    	jetpack_text.visible = false;
    	hasJetpack = true;
    }

     // Check intersection between tank and cur_powerup
    if (Math.abs(runner.position.x - cur_powerup.position.x) < 65 && runner.position.y >= cur_powerup.position.y && cur_powerup.visible) {
    	if (cur_powerup == rapid_fire_sprite) {
    		rapid_fire = true;  // turn on rapid fire
    		shoot_speed = 0;
    	}

    	if (cur_powerup == scatter_shot_sprite) {
    		scatter_shot = true;
    	}

    	if (cur_powerup == speed_boost_sprite) {
    		speed_boost = true;
    		tank_speed = 5;
    	}

    	if (cur_powerup == god_mod_sprite) {
			rapid_fire = true;  // turn on rapid fire
			shoot_speed = 0;

			speed_boost = true; // turn on speed boost
			tank_speed = 5;

			scatter_shot = true; // turn on rapid_fire
    	}

    	powerup_time = new Date().getTime();
    	cur_powerup.visible = false;
    }
}

function circle_intersection(c0, c1) {
	// Use the radius to find the center of the circle:
	var c0x = c0.position.x + c0.radius;
	var c0y = c0.position.y + c0.radius;
	var c1x = c1.position.x + c1.radius;
	var c1y = c1.position.y + c1.radius;

	// Use the distance formula between (c0x, c0y) and (c1x, c1y):
	var cdx = c1x - c0x;
	var cdy = c1y - c0y;
	var d = c0.radius + c1.radius;
	var nd = (cdx * cdx) + (cdy * cdy);
	if (nd < d*d) {
		return true;
	}
	return false;
}

function moveAngryBoss() {
	if (!angry_boss_start) {
		if (Math.abs(player.position.x - runner.position.x) < 200) 
			angry_boss_start = true;
	}
	else {
		// start warning timer
		if (!angry_boss_warning) {
			angry_boss_warning = new Date().getTime();
			bubble.visible = false;
			bubble2.visible = true;
		}


		// don't start moving or shooting til after warning time
		if (new Date().getTime() - angry_boss_warning >= 5000) {

			if (!player.playing)
				player.play();

			bubble2.visible = false;

			// find direction to move
			if (player.position.x <= runner.position.x) {
				bossRight = true;
				bossLeft = false;
				player.scale.x = 1; // flip boss
				player.position.x += 1.5;
			}
			else {
				bossRight = false;
				bossLeft = true;
				player.scale.x = -1;  // flip boss
				player.position.x -= 1.5;
			}

			if (!bossLastShot || new Date().getTime() - bossLastShot >= bossShootSpeed)
				bossShoot();
		}
	}
}

function bossShoot() {
	bossLastShot = new Date().getTime();   // reset last shot time

	boss_bullets[boss_bullets.length] = new PIXI.Sprite(PIXI.Texture.fromImage("bullet.png"));
	boss_bullets[boss_bullets.length-1].visible = true;  // must use bullets.length-1 after adding to array
	boss_bullets[boss_bullets.length-1].position.x = player.position.x + (player.scale.x * 30);
	boss_bullets[boss_bullets.length-1].position.y = player.position.y-58;
	boss_bullets[boss_bullets.length-1].radius = 5;
	entity_layer.addChild(boss_bullets[boss_bullets.length-1]);
	createjs.Tween.get(boss_bullets[boss_bullets.length-1].position).to({x: boss_bullets[boss_bullets.length-1].position.x + (player.scale.x * 1600)}, 2000, createjs.Ease.linear);
	gunshot_sound.play();
}

function game() {
  	if (running) {
		processInput();
		update_camera();
		checkCollisions();
		cleanUp();
		checkPowerUps();
		if (angry_boss)
			moveAngryBoss();
		if (winner)
			game_over();
	}
}

function game_over() {

	if (winner == runner) {    // if player wins
		text = new PIXI.Text("YOU WIN!\n\nPress enter to play again.\nPress ESC to return to the main menu.", {font: "18px Arial"});
	}
	else {
		text = new PIXI.Text("GAME OVER\n\nPress enter to play again.\nPress ESC to return to the main menu.", {font: "18px Arial"});
	}

	player.stop();

	if (runner.position.x > 100 && runner.position.x < game_width - 200)
		text.position.x = runner.position.x - 100;
	else  {
		if (runner.position.x < 100)
			text.position.x = runner.position.x + 100;
		else
			text.position.x = runner.position.x - 200;
	}

	text.position.y = runner.position.y - 375;
	entity_layer.addChild(text);
	running = false;
}

function cleanUp() {   // cleanup bullet and target arrays

	for (var i in bullets) {
		if (bullets[i].position.x < 0 || bullets[i].position.x > game_width)
			bullets.splice(i, 1); // remove bullet that has collided with top of screen
	}

	for (var i in boss_bullets) {
		if (boss_bullets[i].position.x < 0 || boss_bullets[i].position.x > game_width)
			boss_bullets.splice(i, 1); // remove bullet that has collided with top of screen
	}

}

function checkPowerUps() {

	if (!powerup_time || new Date().getTime() - powerup_time > 5000) {
		if (rapid_fire) {
			rapid_fire = false;
			shoot_speed = orig_shoot_speed;
			spawned = false;   // set flag that says ready to spawn powerup
		}

		if (scatter_shot) {
			scatter_shot = false;
			spawned = false;
		}

		if (speed_boost) {
			speed_boost = false;
			tank_speed = orig_tank_speed;
			spawned = false;
		}

	}

	if (new Date().getTime() - powerup_time > 15000 && !spawned) {   // wait 15 seconds and span another powerup
		spawnPowerup();
	}
	
}


function processInput() {

	
	if (goLeft && runner.position.x > 30) {
		//player.position.x -= tank_speed;
		runner.position.x -= tank_speed;
	}

	if (goRight && runner.position.x < game_width-30) {
		//player.position.x += tank_speed;
		runner.position.x += tank_speed;
	}

	if (jumping) {
		if (thrusting)
			jump_speed = 3;

		runner.position.y -= jump_speed;
		jump_speed -= gravity;

		if (runner.position.y >= jump_start) {
			jumping = false;
			runner.position.y = jump_start;
		}
	}

	if (!goLeft && !goRight)
		runner.gotoAndStop(0);

    // update jetpack position to follow player
	if (hasJetpack) {
		jetpack.position.x = runner.position.x - (runner.scale.x * 8);
		jetpack.position.y = runner.position.y - 23;

	  	emitter.spawnPos.x = jetpack.position.x - (runner.scale.x);
	  	emitter.spawnPos.y = jetpack.position.y;
	}
	
}

function animate() {
	requestAnimationFrame(animate);

	if (running) {
		var now = Date.now();
	  
	  	emitter.update((now - elapsed) * 0.001);
	  	elapsed = now;
	}

	renderer.render(current_stage);
	
}

function update() {
	setInterval(game, 15);
}


// add event listeners
document.addEventListener('keydown', keydownListener);
document.addEventListener('keyup', keyupListener);

animate();
update();
})();
